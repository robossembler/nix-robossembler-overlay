# Операционные окружения разработчиков Робосборщика

[Cachix Cache](https://robossembler.cachix.org)

Для установки в свою систему Blender с настройками Robossembler
* `nix profile install gitlab:robossembler/nix-robossembler-overlay#blender`

Запуск локальных окружений для разработки
* Python-окружение для Perception навыков робота: `nix develop gitlab:robossembler/nix-robossembler-overlay#perception`
* ROS2 Rolling `nix develop gitlab:robossembler/nix-robossembler-overlay#ros`

Для внесения изменений в репозиторий:
* `git clone https://gitlab.com/robossembler/nix-robossembler-overlay`
* `cd nix-robossembler-overlay`

# Бинарный кэш

Чтобы ускорить установку (по умолчанию nix будет собирать наши пакеты из исходников), можно использовать бинарный кеш. Для этого добавьте следующие параметры в `/etc/nix/nix.conf`:

```
substituters = https://cache.nixos.org https://ros.cachix.org https://robossembler.cachix.org
trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ros.cachix.org-1:dSyZxI8geDCJrwgvCOHDoAfOm5sV1wCPjBkKL+38Rvo= robossembler.cachix.org-1:56jBJHroRQSGpZFkW8XMquuzQTjAF/XTo6MogmBM7SQ=
```
И перезагрузите nix-daemon:
`sudo systemctl restart nix-daemon.service`

# Библиотека nix-функций

Функции определены в файле `default.nix`. В общем случае для использования функций библиотеки нужно добавить в ваш репозиторий файл `flake.nix` следующего содержания:
```nix
{
  # Входные параметры. В данном случае это библиотека фреймворка Robossembler, через которую подгружаются библиотеки `nixpkgs` и `flake-utils` для синхронизации версий ПО
  inputs = {
    rbs.url = "gitlab:robossembler/nix-robossembler-overlay";
  };

  # Выходные параметры
  outputs = { self, rbs }:
    # Утилиты для вызовов функций библиотеки могут быть специфичны для аппаратной платформы, поэтому наименование аппаратной платформы нужно передать с помощью `flake-utils` 
    rbs.inputs.flake-utils.lib.eachDefaultSystem (system:{
      # Пакеты, которые будут генерироваться. Выражение `with rbs.rlib.${system};` используется, чтобы не прописывать каждый раз функции библиотеки с этим префиксом.
      packages = with rbs.rlib.${system}; {
          # Генерируется пакет `stl` с помощью функции `step2stl`, принимающей в качестве аргументов `<имя пакета>` и путь к файлу, который необходимо конвертировать
          stl = step2stl "gripper" ./gripper.step;
        };
      });
}

```
После чего добавьте файл в git-индекс (`git add flake.nix`) и запустите сборку пакета (`nix build .#stl`). В первый раз команда может выполняться достаточно долго, потому что сборщику требуется время на подгрузку необходимых зависимостей, однако повторный запуск будет почти мгновенным. Ссылка на результат сборки в `/nix/store/` будет находиться в директории `result`.

## Описание функций

### `step2stl`
* Описание: Конвертация файла в формате STEP в файл формата STL. Если вам в своём проекте нужно использовать STL модели, а в проекте родителе фигурирует STEP, то можно обеспечить автоматическую подгрузку STL-модели без необходимости хранить STL в репозитории.
* Параметры: имя пакета строкой (`"name"`), путь к файлу (`./file.step`)
* Пример: `step2stl "gripper" ./gripper.step;`