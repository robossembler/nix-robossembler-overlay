{ stdenv
, bpy
, python3
}:
stdenv.mkDerivation {
  inherit (bpy) version;
  pname = "bpy-lib";
  src = bpy;
  buildInputs = [ python3 ];
  installPhase = ''
    mkdir -p $out/lib/python${python3.pythonVersion}
    ln -s ${bpy}/bpy $out/lib/python${python3.pythonVersion}/site-packages
  '';
}
