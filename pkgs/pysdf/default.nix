{ buildPythonPackage, fetchFromGitHub, python3Packages }:
buildPythonPackage {
  format = "pyproject";
  pname = "pysdf";
  version = "0.4";
  src = fetchFromGitHub {
    owner = "FirefoxMetzger";
    repo = "python-sdformat";
    rev = "51cd5fcb970076f0b64b76938f7923000c8df404";
    sha256 = "sha256-f30rfdvqxB6HE6hDJbthSv679JO6muAHhhVVes/AK3k=";
  };
  propagatedBuildInputs = with python3Packages; [ poetry-core lxml numpy pytest black ];
}
