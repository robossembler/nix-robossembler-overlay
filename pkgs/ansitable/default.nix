{ lib
, buildPythonPackage
, fetchFromGitHub
, colored
, numpy
}:

buildPythonPackage rec {
  pname = "ansitable";
  version = "0.9.7";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "petercorke";
    repo = "ansitable";
    rev = version;
    hash = "sha256-LRSdnHLcmQfwSpxnd3JjaLzpECWWnzi8aWLanu9qRUI=";
  };

  propagatedBuildInputs = [
    colored
    numpy
  ];

  pythonImportsCheck = [ "ansitable" ];

  meta = with lib; {
    description = "Quick, easy and pretty display of tabular data or matrices, with optional ANSI color and borders";
    homepage = "https://github.com/petercorke/ansitable";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
  };
}
