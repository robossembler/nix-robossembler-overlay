{ lib
, stdenv
, fetchFromGitHub
, cmake
, wrapQtAppsHook
}:

stdenv.mkDerivation rec {
  pname = "fstl";
  version = "0.10.0";

  src = fetchFromGitHub {
    owner = "fstl-app";
    repo = "fstl";
    rev = "v${version}";
    hash = "sha256-z2X78GW/IeiPCnwkeLBCLjILhfMe2sT3V9Gbw4TSf4c=";
  };

  nativeBuildInputs = [
    cmake
    wrapQtAppsHook
  ];

  meta = with lib; {
    description = "A fast STL file viewer";
    homepage = "https://github.com/fstl-app/fstl";
    license = with licenses; [ ];
    maintainers = with maintainers; [ ];
  };
}
