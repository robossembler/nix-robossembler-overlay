self: super: with self.lib; let
  
  # single source for Robossembler Framework packages

  rbsFrameworkSrc = self.fetchgit {
    url = "https://gitlab.com/robossembler/framework";
    hash = "sha256-q1032Lu2xnMWZkr+T0rLhbpdmceDHygHC6BN3AKbFVk=";
    rev = "69b8512d6b5dafe0571b5d73a910a26bbc6a84a5";
  };

  pddlSpecGeneratorSrc = self.fetchgit {
    url = "https://gitlab.com/robossembler/pddl-spec-generator";
    hash = "sha256-g/yaaIHRJriPwvVr2pVytQZmJG/BYvsg4q64yEps0T8=";
    rev = "61dcd1553365436f13107f395d1ee7e7b012c3e6";
  };
  
  bpy = self.blender.overrideAttrs (old: {
    pname = "bpy";
    src = self.fetchurl {
      url = "https://download.blender.org/source/${old.pname}-${old.version}.tar.xz";
      hash = "sha256-3AAtguPDQMk4VcZoRzDQGAG2aaKbHMa3XuuZC6aecj8=";
    };
    cmakeFlags = old.cmakeFlags ++ [
      "-DWITH_PYTHON_INSTALL=OFF"
      "-DWITH_AUDASPACE=OFF"
      "-DWITH_PYTHON_MODULE=ON"
      "-DWITH_MEM_JEMALLOC=OFF"
      "-DWITH_INSTALL_PORTABLE=ON"
    ];
    postInstall = ''
      mkdir -p $out/nix-support
      mkdir -p $out/lib/python${self.python3.pythonVersion}/site-packages
      ln -s $out/bpy $out/lib/python${self.python3.pythonVersion}/site-packages/bpy
      echo ${self.python3} >> $out/nix-support/propagated-build-inputs
    '';
  });

in {

  # Robossembler custom packages; defined here for share between devShells and packages

  # General packages

  mayo = self.libsForQt5.callPackage ./mayo { };
  fstl = self.libsForQt5.callPackage ./fstl { };
  blender = self.callPackage ./blender { blender = super.blender.override {
      cudaSupport = true;
      spaceNavSupport = false;
      waylandSupport = false;
    };
  };

  # Python packages



  pythonPackagesExtensions = super.pythonPackagesExtensions ++ [
    (
      python-final: python-prev: {
        iopath = python-prev.iopath.overridePythonAttrs (oldAttrs: {
          version = "unstable";
          src = self.fetchFromGitHub {
            owner = "facebookresearch";
            repo = "iopath";
            rev = "9dcb63a46447556460fdb65c21aa6d094a7a788c";
            hash = "sha256-X1LTOfbIAMy6gToNYS0JpeJpHi0jHPsEjrnCq2c9U0E=";
          };
        });
        unified-planning = self.python3Packages.callPackage ./unified-planning { };
        bpy-wheel = self.python3Packages.callPackage ./bpy-wheel { };
        pysdf = self.python3Packages.callPackage ./pysdf { };
        pytorch3d = self.python3Packages.callPackage ./pytorch3d { };
        blenderproc = self.python3Packages.callPackage ./blenderproc.nix { };
        test-script = self.python3Packages.callPackage ./test-script { };
        webdataset = self.python3Packages.callPackage ./webdataset { };
        ansitable = self.python3Packages.callPackage ./ansitable { };
        megapose6d = self.python3Packages.callPackage ./megapose6d { inherit (python-final) webdataset; };
        spatialmath = self.python3Packages.callPackage ./spatialmath { inherit (python-final) ansitable; };
        rcg-pipeline = self.python3Packages.callPackage ./rcg-pipeline { inherit (python-final) bpy-lib; inherit rbsFrameworkSrc; };
        rbs-workbench = self.python3Packages.callPackage ./freecad.robossembler { inherit (python-final) freecad-lib; inherit rbsFrameworkSrc; };
        freecad-lib = self.python3Packages.toPythonModule (self.callPackage ./freecad-lib { });
        bpy-lib = self.python3Packages.toPythonModule bpy;
      }
    )
  ];

  # Assets Generators
  cad-gen = 
    let
      cad_export_script = self.writers.writePython3 "cad_export_script.py"
        { libraries = [ self.python3Packages.rbs-workbench ]; }
        ''
        import FreeCAD as App
        import freecad.robossembler.utils.freecad_exporters as rbs
        import sys

        path = sys.argv[4]
        print(path)
        doc = App.openDocument(path)
        rbs.publish_project_database(doc)
        App.closeDocument(doc.Name)
        '';
    in
    self.writeShellScriptBin "cad-gen"
      ''
      export PYTHONPATH="${self.python3Packages.rbs-workbench}/${self.python3.sitePackages}"
      ${self.freecad}/bin/freecadcmd ${cad_export_script} -- --pass $1
      '';

  cg-gen = self.writers.writePython3Bin "cg-gen"
    { libraries = [ self.python3Packages.rcg-pipeline ]; }
    ''
    from rcg_pipeline import rcg_full_pipeline
    import sys
    rcg_full_pipeline(sys.argv[1])
    '';

  pddl-spec-gen = pddlSpecGeneratorSrc;
  dataset-gen = rbsFrameworkSrc;

}
