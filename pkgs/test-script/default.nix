{ writers, python3Packages }:
writers.writePython3Bin "test-script" { libraries = []; } ''
import sys
import os
import json


def main():
    arg = sys.argv[1]

    data = json.loads(arg)
    paths = data.get('filesMeta')
    path = data.get('path')
    for el in paths:
        if (el.get('path') is None and el.get('type') == 'folder'):
            if (not os.path.exists((path + el.get('name')))):
                os.makedirs(path + el.get('name'))
        else:
            filePath = el.get('path')
            type = el.get('type')
            name = el.get('name')
            rewrite = el.get('rewrite')
            newPath = path + filePath + '/'
            if (type == 'file'):
                if (rewrite and not os.path.exists((newPath + name))):
                    file = open(newPath + name, 'w')
                    file.write(str(el))
                    file.close()
            if (type == 'folder'):
                if (rewrite and not os.path.exists((newPath + name))):
                    os.makedirs(newPath + name)


main()
''

 