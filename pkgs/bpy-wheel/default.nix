{ buildPythonPackage, fetchPypi, python3Packages, lib, autoPatchelfHook, blender, pythonImportsCheckHook, config, cudaSupport ? config.cudaSupport }:
buildPythonPackage rec {
  format = "wheel";
  pname = "bpy";
  version = "4.1.0";

  src = fetchPypi rec {
    inherit pname version format;
    sha256 = "sha256-bGOlo3NvYMdbXw0UaKGkP7gRVdilKHdwfPtoAF27dLQ=";
    dist = python;
    python = "cp311";
    abi = "cp311";
    platform = "manylinux_2_28_x86_64";
  };
  propagatedBuildInputs = with python3Packages; [ requests numpy cython zstandard ];
  buildInputs = blender.buildInputs;
  nativeBuildInputs = [ autoPatchelfHook pythonImportsCheckHook ];
  autoPatchelfIgnoreMissingDeps = true;
  pythonImportsCheck = [ "bpy" ];

  meta = with lib; {
    description = "3D Creation/Animation/Publishing System";
    homepage = "https://www.blender.org";
    # They comment two licenses: GPLv2 and Blender License, but they
    # say: "We've decided to cancel the BL offering for an indefinite period."
    # OptiX, enabled with cudaSupport, is non-free.
    license = with licenses; [ gpl2Plus ] ++ optional cudaSupport unfree;
    platforms = [ "x86_64-linux" ];
    maintainers = with maintainers; [ brothermechanic ];
  };
}
