{ lib
, python3
, fetchFromGitHub
, clingo
}:
let
  buildPythonPackage = python3.pkgs.buildPythonPackage;

  antlr472-python3-runtime = buildPythonPackage rec {
    pname = "antlr4-python3-runtime";
    version = "v4.7.2";
    src = builtins.fetchTarball {
      url = "https://files.pythonhosted.org/packages/29/14/8ac135ec7cc9db3f768e2d032776718c6b23f74e63543f0974b4873500b2/antlr4-python3-runtime-4.7.2.tar.gz";
      sha256 = "0m7axw4fys10da5bwpz39nkdji15z8jka3w11m5dx0ym0lag0qvh";
    };
    meta = with lib; {
      description = "Runtime for ANTLR";
      homepage = "https://www.antlr.org/";
      license = licenses.bsd3;
    };
  };

  tarski = buildPythonPackage rec {
    pname = "tarski";
    version = "0.8.2";
    format = "pyproject";

    src = fetchFromGitHub {
      owner = "aig-upf";
      repo = "tarski";
      rev = "v${version}";
      hash = "sha256-9yXFPrfLXNUWSoTuMKgdDxtbCZCvrcRflZHJLnJUPAw=";
    };

    nativeBuildInputs = with python3.pkgs; [
      cython
      setuptools
      wheel
    ];

    propagatedBuildInputs = (with python3.pkgs; [ psutil multipledispatch ]) ++ [ antlr472-python3-runtime ];

    pythonImportsCheck = [ "tarski" ];

    meta = with lib; {
      description = "Tarski - An AI Planning Modeling Framework";
      homepage = "https://github.com/aig-upf/tarski";
      changelog = "https://github.com/aig-upf/tarski/blob/${src.rev}/CHANGELOG.md";
      license = licenses.asl20;
      maintainers = with maintainers; [ ];
    };
  };
in
buildPythonPackage rec {
  pname = "unified-planning";
  version = "1.0.0";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "aiplan4eu";
    repo = "unified-planning";
    rev = "f9eba2544d15b26f91880c1e5e274e8597276ba7";
    hash = "sha256-ZBZZQFXCe0q5bPx5wNMCmj5ckGoWi9ybUY41NNtWPLo=";
  };

  propagatedBuildInputs = (with python3.pkgs; [
    networkx
    pyparsing
    pytest
    protobuf
    tarski
    grpcio
    grpcio-tools
  ]);

  checkPhase = ''
    export PATH=${lib.makeBinPath [ clingo ]}:$PATH
  '';

  pythonImportsCheck = [ "unified_planning" ];

  meta = with lib; {
    description = "The AIPlan4EU Unified Planning Library";
    homepage = "https://github.com/aiplan4eu/unified-planning";
    license = licenses.asl20;
    maintainers = with maintainers; [ movefasta  ];
  };
}
