{ stdenv
, freecad
, python3
}:
stdenv.mkDerivation {
  inherit (freecad) version;
  pname = "freecad-lib";
  src = freecad;
  buildInputs = [ python3 ];
  installPhase = ''
    mkdir -p $out/lib/python${python3.pythonVersion}
    ln -s ${freecad}/lib $out/lib/python${python3.pythonVersion}/site-packages
  '';
}
