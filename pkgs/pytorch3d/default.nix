{ lib
, pkgs
, buildPythonPackage
, fetchFromGitHub
, torch
, fvcore
, pyopengl
, pyopengl-accelerate
, pybind11
, python3
, scikit-image
, black
, usort
, flake8
, matplotlib
, tqdm
, jupyter
, imageio
, plotly
, opencv
, iopath
, ninja
}:
buildPythonPackage rec {
  pname = "pytorch3d";
  version = "0.7.3";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "facebookresearch";
    repo = "pytorch3d";
    rev = "v${version}";
    hash = "sha256-lI3z4Z7j4zJNSXsqueBQqBe8gslmLJRjW4DM8CQ9A0A=";
  };

  preBuild = ''
    export MAX_JOBS=$NIX_BUILD_CORES;
  '';

  nativeBuildInputs = with pkgs; [ which pybind11 ninja ];

  propagatedBuildInputs = [
    torch
    fvcore
    iopath
    pyopengl
    pyopengl-accelerate
    pybind11
    scikit-image
    black
    usort
    flake8
    matplotlib
    tqdm
    jupyter
    imageio
    plotly
    opencv
  ];

  pythonImportsCheck = [ "pytorch3d" ];
  doCheck = false;
  
  enableParallelBuilding = true;

  meta = with lib; {
    description = "PyTorch3D is FAIR's library of reusable components for deep learning with 3D data";
    homepage = "https://github.com/facebookresearch/pytorch3d";
    license = licenses.bsd3;
    maintainers = with maintainers; [ ];
  };
}
