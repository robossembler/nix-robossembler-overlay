{ fetchgit, fetchzip, blender, imath, git, lib, ... }:
let
  cg_preferences = fetchgit {
    url = "https://gitflic.ru/project/brothermechanic/cg_preferences.git";
    sha256 = "sha256-FkF1c5MhqtG2COIpSBdoJ1aE0r8/ABtmqFLqNkF0zT0=";
  };

  rs_logo = fetchgit {
    url = "https://gitlab.com/robossembler/logo.git";
    rev = "0a0235cd154a59b711e87456206ccd7ba69a7e4e";
    sha256 = "sha256-/QsgKdtDHNymsTEmhIzSrOKNUqqkXl8coy+5LwIEkas=";
    fetchLFS = true;
  };
in
blender.overrideAttrs (old: {
    optix = fetchzip {
      # url taken from the archlinux blender PKGBUILD
      url = "https://developer.download.nvidia.com/redist/optix/v7.4/OptiX-7.4.0-Include.zip";
      sha256 = "sha256-ca08XetwaUYC9foeP5bff9kcDfuFgEzopvjspn2s8RY=";
    };
    patches = old.patches ++ [ "${cg_preferences}/patches/cg-defaults.patch" ];
    buildInputs = old.buildInputs ++ [ imath git ];
    postPatch = old.postPatch + ''
      cp ${cg_preferences}/share/00_cg_preferences_service.py scripts/startup/
      cp ${cg_preferences}/share/startup.blend release/datafiles/
      cp ${rs_logo}/raster/robossembler_1024x512_roboty-rossii.png  release/datafiles/splash.png
    '';
    env.NIX_CFLAGS_COMPILE = old.env.NIX_CFLAGS_COMPILE + " -I${imath}/include/Imath";
    postInstall = old.postInstall + ''
      cp -r ${cg_preferences}/cg_preferences $out/share/blender/${
        lib.versions.majorMinor blender.version
      }/scripts/addons/
    '';
  })