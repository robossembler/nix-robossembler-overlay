{ lib
, buildPythonPackage
, fetchFromGitHub
, setuptools
, ansitable
, matplotlib
, numpy
, scipy
, typing-extensions
, coverage
, flake8
, pytest
, pytest-timeout
, pytest-xvfb
, sympy
, sphinx
, sphinx-autodoc-typehints
, sphinx-rtd-theme
, sphinxcontrib-jsmath
, oldest-supported-numpy
}:

buildPythonPackage rec {
  pname = "spatialmath";
  version = "unstable-2023-06-07";
  format = "pyproject";

  src = fetchFromGitHub {
    owner = "petercorke";
    repo = "spatialmath-python";
    rev = "0a7761dcd563e91eb55e75d8f0b2a93198762315";
    hash = "sha256-kawe9yHX7T+Bvy7BfuKbotEjlnpynQAooBsynGJSfjo=";
  };

  nativeBuildInputs = [
    setuptools
    oldest-supported-numpy
  ];

  propagatedBuildInputs = [
    ansitable
    matplotlib
    numpy
    scipy
    typing-extensions
  ];

  passthru.optional-dependencies = {
    dev = [
      coverage
      flake8
      pytest
      pytest-timeout
      pytest-xvfb
      sympy
    ];
    docs = [
      sphinx
      sphinx-autodoc-typehints
      sphinx-rtd-theme
      sphinxcontrib-jsmath
    ];
  };

  pythonImportsCheck = [ "spatialmath" ];

  meta = with lib; {
    description = "Create,  manipulate and convert representations of position and orientation in 2D or 3D using Python";
    homepage = "https://github.com/petercorke/spatialmath-python";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
  };
}
