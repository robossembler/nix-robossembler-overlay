{ buildPythonPackage
, fetchgit
, python3Packages
, lib
, autoPatchelfHook
, pythonImportsCheckHook
, config
, setuptools
, freecad-lib
, pytestCheckHook
, toPythonModule
, python
, rbsFrameworkSrc
, ... }:

buildPythonPackage rec {
  name = "freecad.robossembler";
  version = "1.0.0";
  format = "setuptools";

  src = rbsFrameworkSrc;

  sourceRoot = "${src.name}/freecad_workbench";

  nativeBuildInputs = [
    setuptools
  ];

  buildInputs = [ freecad-lib ];

  propagatedBuildInputs = [
    freecad-lib
  ];
  
  pythonImportsCheck = [ "freecad.robossembler" ];

  doCheck = false;
  
  meta = with lib; {
    description = "Robossembler CG Pipeline Python Module";
    homepage = "https://gitlab.com/robossembler/framework/rcg_pipeline";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
  };
}
