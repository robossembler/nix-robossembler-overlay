{ lib
, stdenv
, fetchFromGitHub
, qtbase
, qmake
, wrapQtAppsHook
, opencascade-occt
}:

stdenv.mkDerivation rec {
  pname = "mayo";
  version = "0.8.0";

  src = fetchFromGitHub {
    owner = "fougue";
    repo = "mayo";
    rev = "v${version}";
    hash = "sha256-/MavLqC3BBRmlqMZIhKEYJPTV6pvLKlCNMXGJ6vnmUg=";
  };

  buildInputs = [ qtbase opencascade-occt ];
  nativeBuildInputs = [ wrapQtAppsHook qmake ];
  propagatedBuildInputs = [ qtbase ];
  
  buildPhase = ''
    export CASCADE_INC_DIR=${opencascade-occt}/include/opencascade
    export CASCADE_LIB_DIR=${opencascade-occt}/lib
    qmake ${src} CASCADE_INC_DIR=$CASCADE_INC_DIR CASCADE_LIB_DIR=$CASCADE_LIB_DIR
    make -j $NIX_BUILD_CORES
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp mayo $out/bin
  '';

  meta = with lib; {
    description = "3D CAD viewer and converter based on Qt + OpenCascade";
    homepage = "https://github.com/fougue/mayo";
    license = licenses.bsd2;
    maintainers = with maintainers; [ movefasta  ];
  };
}
