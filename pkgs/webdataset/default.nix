{ lib
, buildPythonPackage
, fetchFromGitHub
, python3
, braceexpand
, dataclasses-json
, numpy
, pyyaml
}:

buildPythonPackage rec {
  pname = "webdataset";
  version = "0.2.49";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "webdataset";
    repo = "webdataset";
    rev = version;
    hash = "sha256-ZIe3fjtilMsY4fCG7E1OWERFFjFg86KYTTEq/dZDjOM=";
  };

  propagatedBuildInputs = [
    braceexpand
    dataclasses-json
    numpy
    pyyaml
  ];

  pythonImportsCheck = [ "webdataset" ];

  checkPhase = ''
    runHook preCheck
    ${python3.interpreter} -m unittest
    runHook postCheck
  '';

  meta = with lib; {
    description = "A high-performance Python-based I/O system for large (and small) deep learning problems, with strong support for PyTorch";
    homepage = "https://github.com/webdataset/webdataset";
    license = licenses.bsd3;
    maintainers = with maintainers; [ ];
  };
}
