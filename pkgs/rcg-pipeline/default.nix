{ buildPythonPackage
, fetchgit
, python3Packages
, lib
, autoPatchelfHook
, pythonImportsCheckHook
, config
, cudaSupport ? config.cudaSupport
, numpy
, setuptools
, bpy-lib
, rbsFrameworkSrc
, ... }:

buildPythonPackage rec {
  name = "rcg_pipeline";
  version = "1.0.0";
  format = "pyproject";

  src = rbsFrameworkSrc;

  sourceRoot = "${src.name}/rcg_pipeline";

  nativeBuildInputs = [
    setuptools
  ];

  propagatedBuildInputs = [
    numpy
    bpy-lib
  ];

  # TODO - see later, pyproject.toml duntime deps check doesnt work
  dontCheckRuntimeDeps = true;

  pythonImportsCheck = [ "rcg_pipeline" ];

  meta = with lib; {
    description = "Robossembler CG Pipeline Python Module";
    homepage = "https://gitlab.com/robossembler/framework/rcg_pipeline";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
  };
}
