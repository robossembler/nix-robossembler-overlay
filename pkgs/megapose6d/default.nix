{ lib
, buildPythonPackage
, python3Packages
, fetchFromGitHub
, webdataset
}:

buildPythonPackage rec {
  pname = "megapose6d";
  version = "unstable-2023-03-03";
  format = "pyproject";

  src = fetchFromGitHub {
    owner = "megapose6d";
    repo = "megapose6d";
    rev = "293331902f8fa73a5a3f6995cc5b7f42d893f550";
    hash = "sha256-f5pF8gOycksnW/qQoUmhqDrlv7gqR70dR5ppnPunb1c=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = with python3Packages; [ setuptools_scm ];

  propagatedBuildInputs = with python3Packages; [
    setuptools
    wheel
    opencv4
    bokeh
    eigenpy
    pinocchio
    torch
    transforms3d
    omegaconf
    joblib
    tqdm
    imageio
    pypng
    trimesh
    torchvision
    simplejson
    pyroma
    seaborn
    webdataset
    # panda3d
    # open3d
  ];

  pythonImportsCheck = [ "megapose" ];

  meta = with lib; {
    description = "Code for \"MegaPose: 6D Pose Estimation of Novel Objects via Render & Compare\", CoRL 2022";
    homepage = "https://github.com/megapose6d/megapose6d";
    license = licenses.asl20;
    maintainers = with maintainers; [ ];
  };
}
