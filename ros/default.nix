self: super:
{
  humble = super.rosPackages.humble.overrideScope (
    final: prev: {
      # remove after update github:lopsided98/nixpkgs/nix-ros for https://github.com/NixOS/nixpkgs/pull/224099
      behaviortree-cpp-v3 = prev.behaviortree-cpp-v3.overrideAttrs (old: {
        propagatedBuildInputs = old.propagatedBuildInputs ++ [ self.zeromq self.pkg-config ];
      });
      moveit-core = prev.moveit-core.overrideAttrs (old: { 
          propagatedBuildInputs = old.propagatedBuildInputs ++ [ prev.tf2-kdl ];
      });
      moveit-kinematics = prev.moveit-kinematics.overrideAttrs (old: { 
          propagatedBuildInputs = old.propagatedBuildInputs ++ [ final.moveit-ros-planning ];
      });
    }
  );
}
