{ mkDerivation, pkgs, mayo, wrapQtAppsHook, lib, rbs-workbench, cad-gen, cg-gen }:
let
  step2stl = 
    name: file:
    let
      pname = "${name}.stl";
      extension = lib.last (lib.strings.splitString "." file);
    in
    lib.throwIfNot
      (lib.elem extension ["step" "STEP" "stp"])
      ''Input file have a "${extension}" extension, but valid STEP-file should have one of the following extentions: "step" "STEP" "stp"''
    mkDerivation {
      inherit pname;
      version = "v0";
      src = ./.;
      nativeBuildInputs = [ mayo wrapQtAppsHook ];
      buildPhase = ''${lib.getExe mayo} ${file} -e ${pname}'';
      installPhase = ''
        cp ${pname} $out
        rm ${pname}
      '';
    };

  freecad2parts =
    file:
    let
      pname = "${file}-parts";
      extension = lib.last (lib.strings.splitString "." file);
      cad_export_script = pkgs.writers.writePython3 "cad_export_script.py"
        { libraries = [ rbs-workbench ]; }
        ''
        import FreeCAD as App
        import freecad.robossembler.utils.freecad_exporters as rbs
        import sys

        path = sys.argv[4]
        print(path)
        doc = App.openDocument(path)
        rbs.publish_project_database(doc)
        App.closeDocument(doc.Name)
        '';
    in
    lib.throwIfNot
      (lib.elem extension ["FCStd"])
      ''Input file have a "${extension}" extension, but valid FreeCAD file should have "FCStd" extention''
    mkDerivation rec {
      inherit pname;
      version = "unstable";
      src = ./.;
      nativeBuildInputs = [ cad-gen pkgs.freecad ] ++ pkgs.freecad.buildInputs;
      buildInputs = [ ];
      dontWrapQtApps = true;
      buildPhase = ''
        export PYTHONPATH="${rbs-workbench}/${pkgs.python3.sitePackages}"
        ${pkgs.freecad}/bin/freecadcmd ${cad_export_script} -- --pass $1
        #${pkgs.freecad}/bin/freecadcmd -h
        #${cad-gen}/bin/cad-gen ${file}
        mv parts* trees* $out
      '';
    };

  parts-packages =
    parts-package:
    let
      parts = lib.trivial.importJSON "${parts-package.src}/parts.json";
      pkgSet = builtins.listToAttrs (map ({ material_path, name, part_path }: { inherit name; value = part_path; }) parts );
      mkPkg =
        { name, value }:
        mkDerivation {
          src = "${parts-package}";
          pname = name;
          installPhase = ''
            outputs = [ "stl" ];
            ln ${parts-package}/${value} $stl
          '';
        };
    in
    lib.lists.forEach mkPkg pkgSet;

  rlib = { inherit step2stl freecad2parts parts-packages; };
in
rlib
