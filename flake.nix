{
  description = "Robossembler Development Environments on Nix";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=master";
    flake-utils.follows = "ros-overlay/flake-utils";
    ros-overlay.url = "github:lopsided98/nix-ros-overlay?ref=develop";
    ros-nixpkgs.follows = "ros-overlay/nixpkgs";
    gazebo-overlay = {
      url = "github:movefasta/gazebo-sim-overlay";
      inputs.flake-utils.follows = "ros-overlay/flake-utils";
      inputs.nixpkgs.follows = "ros-overlay/nixpkgs";

      # Transitive inputs "not in registry" https://github.com/NixOS/nix/issues/5790
      # inputs.nixgl.inputs.nixpkgs.follows = "ros-overlay/nixpkgs";
      # inputs.nixgl.inputs.flake-utils.follows = "ros-overlay/flake-utils";
    };
  };

  outputs = { self, nixpkgs, ros-overlay, ros-nixpkgs, gazebo-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system;
            overlays = [ 
              (import ./overlay.nix)
              gazebo-overlay.overlays.default
            ];
            config.allowUnfree = true; # for cuda
          };
          ros = import ros-nixpkgs { inherit system;
            overlays = [
              ros-overlay.overlays.default
              (import ./ros)
            ];
          };
        in
        {

          rlib = import ./. { 
            inherit pkgs;
            inherit (pkgs) lib mayo rbs-workbench cad-gen cg-gen;
            inherit (pkgs.stdenv) mkDerivation;
            inherit (pkgs.libsForQt5) wrapQtAppsHook;
          };

          packages = {
            inherit (pkgs) cad-gen cg-gen blender dataset-gen pddl-spec-gen;
          };

          inherit ros pkgs;

          devShells = {
            default =
            let
              my-python-packages = p: with p; [ 
                unified-planning
                pybullet
                scipy
                spatialmath
                rcg-pipeline
                rbs-workbench
              ];
            in
            pkgs.mkShell {
              packages = (with pkgs; [ freecad mayo ]) ++ [ (pkgs.python3.withPackages my-python-packages) ];
            };

            ros = pkgs.mkShell {
              name = "ROS environment";
              packages = [
                ros.colcon
                (with ros.rosPackages.humble; buildEnv {
                  paths = [
                    ros-core
                    rmw-fastrtps-dynamic-cpp
                  ];
                })
              ];
              # ROS 2 middleware variants, for Cyclone DDS replace with "rmw_cyclonedds_cpp" and add rmw-cyclonedds-cpp to paths
              RMW_IMPLEMENTATION = "rmw_fastrtps_dynamic_cpp";
            };

            all = pkgs.mkShell { packages = with pkgs; [ fstl mayo cad-gen cg-gen ]; };

            ci = pkgs.mkShell { buildInputs = [ pkgs.jq ]; };

            gazebo = gazebo-overlay.devShells.${system}.default;

            perception =
              let
                perception-packages = p: with p; [
                  torch
                  torchvision
                  torchaudio
                  detectron2
                ];
              in
              pkgs.mkShell {
                packages = [ (pkgs.python3.withPackages perception-packages) ];
              };
          }; 
        }
      );

  nixConfig = {
    extra-substituters = [ "https://ros.cachix.org" "https://robossembler.cachix.org" ];
    extra-trusted-public-keys = [ 
      "ros.cachix.org-1:dSyZxI8geDCJrwgvCOHDoAfOm5sV1wCPjBkKL+38Rvo="
      "robossembler.cachix.org-1:56jBJHroRQSGpZFkW8XMquuzQTjAF/XTo6MogmBM7SQ="
    ];
  };
}
